import HTTP from './http';

export const getParam = function (params) {
    if (params == null || !params) {
      return "";
    }
  
    const result = Object.entries(params)
      .map(([key, value]) => {
        if (value == null || value == undefined) {
          return `${key}=`;
        }
        return `${key}=${encodeURIComponent(value)}`;
      })
      .join("&");
  
    return "?" + result;
};

export const get = (url, params) => {
    return HTTP.get(`${process.env.VUE_APP_API_URL}${url}${getParam(params)}`);
}
